function init()
{
   select = defaultSelection;
   rList = [];
   var _loc1_ = 0;
   while(_loc1_ < 22)
   {
      var _loc2_ = carte["r" + _loc1_];
      rList.push(_loc2_);
      _loc2_.id = _loc1_;
      _loc2_.name = REGION_NAMES[_loc1_];
      var _loc3_ = 1793987;
      if(select == _loc1_)
      {
         _loc3_ = 16711680;
      }
      setPercentColor(_loc2_,100,_loc3_);
      _loc1_ = _loc1_ + 1;
   }
   title.field.text = REGION_NAMES[select];
   initRanking();
}
function initRanking()
{
   var _loc9_ = ranking.split("|");
   var _loc6_ = 156;
   var _loc3_ = 0;
   while(_loc3_ < _loc9_.length)
   {
      var _loc7_ = _loc9_[_loc3_];
      var _loc2_ = _loc7_.split(",");
      var _loc1_ = attachMovie("mcSlot","slot" + _loc3_,_loc3_);
      _loc1_._x = 0;
      _loc1_._y = _loc6_;
      var _loc5_ = 1793987;
      var _loc4_ = 13;
      if(_loc2_[2] != null)
      {
         _loc5_ = 16711680;
         var _loc8_ = 1.5;
         _loc1_.field.textColor = 16763071;
         _loc1_._xscale = _loc1_._yscale = 100 * _loc8_;
         _loc1_._x = -32.5;
         _loc6_ = _loc6_ + 9;
         _loc4_ = 11;
      }
      _loc4_ = _loc4_ - _loc2_[0].length;
      _loc1_.field.text = _loc2_[0] + " - " + _loc2_[1].substr(0,_loc4_);
      _loc1_.but.onRelease = clickOut;
      _loc1_.url = "http://" + _loc2_[1] + ".miniville.fr";
      glow(_loc1_,2,6,_loc5_);
      _loc6_ = _loc6_ + 12;
      _loc3_ = _loc3_ + 1;
   }
}
function clickOut()
{
   getURL(this._parent.url,"_self");
}
function setPercentColor(mc, prc, col)
{
   var _loc2_ = {r:col >> 16,g:col >> 8 & 255,b:col & 255};
   var _loc4_ = new Color(mc);
   var _loc1_ = prc / 100;
   var _loc5_ = {ra:int(100 - prc),ga:int(100 - prc),ba:int(100 - prc),aa:100,rb:int(_loc1_ * _loc2_.r),gb:int(_loc1_ * _loc2_.g),bb:int(_loc1_ * _loc2_.b),ab:0};
   _loc4_.setTransform(_loc5_);
}
function glow(mc, b, s, c)
{
   var _loc1_ = new flash.filters.GlowFilter();
   _loc1_.blurX = b;
   _loc1_.blurY = b;
   _loc1_.strength = s;
   _loc1_.color = c;
   var _loc2_ = mc.filters;
   _loc2_.push(_loc1_);
   mc.filters = _loc2_;
}
REGION_NAMES = ["Nord Pas-de-Calais","Haute-Normandie","Picardie","Champagne-Ardenne","Lorraine","Alsace","Basse-Normandie","Ile-de-France","Bretagne","Pays de la Loire","Centre","Bourgogne","Franche-Comte","Poitou-Charente","Limousin","Auvergne","Rhones-Alpes","Aquitaine","Midi-Pyrenées","Languedoc Roussillon","Provence Côte d\'Azur","Corse"];
init();
